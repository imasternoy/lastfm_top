# Task
Write a service, which accepts the path to the dataset and produces a list of top 10 songs played in the top 50 longest user sessions by tracks count. Each user session may be comprised of one or more songs played by that user, where each song is started within 20 minutes of the previous song's start time.

# Requirements

The task must be solved using either Java, Scala or Clojure programming languages. You're free to use any framework/3rd party lib of your choice

As a 3d party library I've choosed Apache Spark 2.3 as it has a lot of functionality transforming big datasets.

# Installation

```sh
$ git clone https://bitbucket.org/imasternoy/lastfm_top.git
$ mvn clean install
$ cd target/spark-test-0.0.1-bin/spark-test-0.0.1
```
Setup Spark environment and Input params:
```sh
$ export SPARK_BIN=
$ export SPARK_MASTER=
$ export INPUT_FILE=
```
Application was tested with Spark 2.3.0 available here: 
```
$ curl http://apache.cp.if.ua/spark/spark-2.3.0/spark-2.3.0-bin-hadoop2.7.tgz > spark-2.3.0-bin-hadoop2.7.tgz
```
If you don't have a task input file you can download it using:
```sh
$downloadFile.sh
```
After that run an application using
```sh
$ spark-submit.sh
```
Resulting folder top10songs with top 10 Songs CSV file should be created after successfull job execution in the launch folder. 

# Algo

Algorithm should works in following way:
- Create required user sessions dataset. Which means we need to aggregate initial dataset by user and combine all timestamps, comparing the next timestamp with previous. Session is formed if next song launch time timestamp is less then 20 minutes;
- After that we generating top 50 session by amount of songs in it
- With the list of top sessions we can filter songs by the session and count each song playback