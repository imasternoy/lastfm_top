package com.masternoy.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.expressions.WindowSpec;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

public class SparkTest {

	public static void main(String[] args) {
		// Initial sanity check
		if (args.length < 1 || args[0].isEmpty()) {
			System.err.println("Please, setup input file name");
			System.exit(1);
		}
		String fileName = args[0];
		// Initialize Spark Context
		JavaSparkContext sc = new JavaSparkContext(new SparkConf().setAppName("TOP10_LastFM").setMaster("local[4]"));
		SparkSession session = SparkSession.builder().getOrCreate(); //

		StructType structure = new StructType( //
				new StructField[] { //
						new StructField("userId", DataTypes.StringType, true, Metadata.empty()), //
						new StructField("startTime", DataTypes.TimestampType, true, Metadata.empty()), //
						new StructField("auid", DataTypes.StringType, true, Metadata.empty()), //
						new StructField("artist", DataTypes.StringType, true, Metadata.empty()), // 
						new StructField("suid", DataTypes.StringType, true, Metadata.empty()), // Sometimes we have null song uid field
						new StructField("song", DataTypes.StringType, true, Metadata.empty()) // Sometimes we have null song name
				});//Not sure if we need all the properties

		Dataset<Row> df = session.read().schema(structure).option("sep", "\t").csv(fileName);
		df.show();

		WindowSpec userWindow = Window.partitionBy("userId").orderBy("startTime");
		WindowSpec userSessionWindow = Window.partitionBy("userId", "session");

		// Select sessions which diff between songs <= 20 min
		Column session_col = functions.coalesce( //
				functions.unix_timestamp(functions.col("startTime")).minus( //
						functions.unix_timestamp(functions.lag(functions.col("startTime"), 1).over(userWindow))), //
				functions.lit(0)) //
				.$greater(20 * 60) // The window starts if next song has been started >= 20 minutes
				.cast(DataTypes.IntegerType); //
		//
		Dataset<Row> df2 = df.withColumn("session", functions.sum(session_col).over(userWindow))//
				.withColumn("songs", functions.count("song").over(userSessionWindow)) //
				.drop("auid") // Remove unused cols
				.drop("suid") // Remove unused cols
				.drop("artist")// Remove unused cols
				.orderBy(functions.col("songs").desc(), functions.col("startTime").desc()); // ;

		Dataset<Row> dfPersisted = df2.persist();// Persist as we gonna reuse it
		df2.show(20, true);//Debug info

		// Now select top 50 users sessions by amount of songs in it
		Dataset<Row> topSessions = dfPersisted.select(functions.col("session"),functions.col("songs")).groupBy(functions.col("session")) //
				.max("songs") //
				.orderBy(functions.col("max(songs)").desc())//
				.limit(50);// limiting top 50 sessions

		Dataset<Row> topSSPersisted = topSessions.persist();// Persist as we gonna reuse it
		topSSPersisted.show(50);// Debug info
		sc.broadcast(topSSPersisted);// Broadcast topSessions to each worker

		// Select top 10 songs from top 50 sessions
		// Get count of each song playback ordering by song name
		dfPersisted.select(functions.col("song"),functions.col("session")) //
				.filter(functions.col("session") //
						.isin(topSSPersisted.col("session"))) //
				.groupBy(functions.col("song")).count() //
				.orderBy(functions.col("count").desc()) //
				// Show output
				.repartition(1).limit(10).write() //
				.option("header", "true") //
				.csv("top10songs"); //

		sc.stop();
		sc.close();
	}

}
