#!/bin/sh
echo "Creating data folder.."
mkdir data
cd data
echo "Downloading file..."
curl http://mtg.upf.edu/static/datasets/last.fm/lastfm-dataset-1K.tar.gz > lastfm-dataset-1K.tar.gz
echo "Unzipping..."
tar -xvf lastfm-dataset-1K.tar.gz