#!/bin/sh
: "${SPARK_BIN:?Need to set SPARK_BIN non-empty}"
: "${SPARK_MASTER:?Need to set SPARK_MASTER non-empty}"
: "${INPUT_FILE:?Need to set INPUT_FILE non-empty}"

echo "Launching program with paramms: \n Spark-bin: ${SPARK_BIN}, Spark-master: ${SPARK_MASTER}"
#spark://spark-master:7077
#"./data/lastfm-dataset-1K/userid-timestamp-artid-artname-traid-traname.tsv"
${SPARK_BIN}/spark-submit --class com.masternoy.spark.SparkTest --master ${SPARK_MASTER} spark-test.jar ${INPUT_FILE}
